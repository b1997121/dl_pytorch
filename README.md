```python
import torch
import numpy
```


# [TORCH.TENSOR](https://pytorch.org/docs/stable/generated/torch.tensor.html#torch-tensor)
> Create tensor

```python
>>> a = torch.Tensor([1, 2])
>>> a
tensor([1., 2.])
```

# [TORCH.TENSOR.TYPE](https://pytorch.org/docs/stable/generated/torch.Tensor.type.html#torch-tensor-type)
> Check tensor type

```python
>>> a = torch.Tensor([1, 2])
>>> a.type()
'torch.FloatTensor'
```

> Specific tensor data type
```python
a = torch.tensor([1, 2], dtype=torch.double)
a = torch.tensor([1, 2]).double()
a = torch.tensor([1, 2]).to(torch.double)
```


# [TORCH.ZEROS](https://pytorch.org/docs/stable/generated/torch.zeros.html#torch-zeros)
> Returns a tensor filled with the scalar value 0, with the shape 2 x 3

```python
>>> a = torch.zeros(2, 3)
>>> a
tensor([[0., 0., 0.],
        [0., 0., 0.]])
```

# (TORCH.ONES)[https://pytorch.org/docs/stable/generated/torch.ones.html#torch-ones]
> Returns a tensor filled with the scalar value 1, with the shape 2 x 3

```python
>>> a = torch.ones(2, 3)
>>> a
tensor([[1., 1., 1.],
        [1., 1., 1.]])
```


# [TORCH.RAND](https://pytorch.org/docs/stable/generated/torch.rand.html#torch-rand)
> Returns a tensor filled with random numbers from a uniform distribution on the interval (0,1)

```python
>>> a = torch.rand(2, 3)
>>> a
tensor([[0.9749, 0.1368, 0.7942],
        [0.6808, 0.9260, 0.5618]])
```


# [TORCH.RANDN](https://pytorch.org/docs/stable/generated/torch.randn.html#torch-randn)
> Returns a tensor filled with random numbers from a normal distribution with mean 0 and variance 1

```python
>>> a = torch.randn(2, 3)
>>> a
tensor([[ 0.8876,  0.0408, -0.4331],
        [ 1.0735, -0.4171, -0.4068]])
```


# [TORCH.MANUAL_SEED](https://pytorch.org/docs/stable/generated/torch.manual_seed.html#torch-manual-seed)
> Sets the seed for generating random numbers. Returns a torch.Generator object

```python
>>> torch.manual_seed(42)
<torch._C.Generator object at 0x7fef20755750>
>>> a = torch.rand(2, 3)
>>> a
tensor([[0.8823, 0.9150, 0.3829],
        [0.9593, 0.3904, 0.6009]])
>>> torch.manual_seed(42)
<torch._C.Generator object at 0x7fef20755750>
>>> b = torch.rand(2, 3)
>>> b
tensor([[0.8823, 0.9150, 0.3829],
        [0.9593, 0.3904, 0.6009]])
```


# [TORCH.FROM_NUMPY](https://pytorch.org/docs/stable/generated/torch.from_numpy.html#torch-from-numpy)
> Creates a Tensor from a numpy.ndarray

```python
>>> a = numpy.array([1, 2, 3])
>>> torch.from_numpy(a)
tensor([1, 2, 3])
```

> Creates a numpy.ndarray from a Tensor

```python
>>> t = torch.Tensor([1, 2])
>>> t.numpy()
array([1., 2.], dtype=float32)
```


# [TORCH.CUDA.IS_AVAILABLE](https://pytorch.org/docs/stable/generated/torch.cuda.is_available.html#torch-cuda-is-available)
> Returns a bool indicating if CUDA is currently available

```python
>>> torch.cuda.is_available()
False
```


# [TORCH.TENSOR.SIZE](https://pytorch.org/docs/stable/generated/torch.Tensor.size.html#torch-tensor-size)
> Returns the size of the self tensor

```python
>>> t = torch.rand(3, 5)
>>> t.size()
torch.Size([3, 5])
>>> t.size(dim=1)
5
```


# [Tensor.mT](https://pytorch.org/docs/stable/tensors.html#torch.Tensor.mT)
> Returns a view of this tensor with the last two dimensions transposed

```python
>>> a = torch.rand(2, 3)
>>> a
tensor([[0.4414, 0.2969, 0.8317],
        [0.1053, 0.2695, 0.3588]])
>>> a.mT
tensor([[0.4414, 0.1053],
        [0.2969, 0.2695],
        [0.8317, 0.3588]])
```


# [TORCH.PERMUTE](https://pytorch.org/docs/stable/generated/torch.permute.html#torch-permute)
> Returns a view of the original tensor input with its dimensions permuted

```python
>>> x = torch.randn(2, 3, 5)
>>> x
tensor([[[ 0.2415, -1.1109,  0.0915, -2.3169, -0.2168],
         [-1.3847, -0.8712, -0.2234, -0.6216, -0.5920],
         [-0.0631, -0.8286,  0.3309, -1.5576, -0.9224]],

        [[ 1.8113,  0.1606,  0.3672,  0.1754,  1.3852],
         [-0.4459,  1.4451,  0.7078, -1.0759,  0.5357],
         [ 1.1754,  0.5612, -0.4527, -0.7718, -0.1722]]])
>>> torch.permute(x, (2, 0, 1))
tensor([[[ 0.2415, -1.3847, -0.0631],
         [ 1.8113, -0.4459,  1.1754]],

        [[-1.1109, -0.8712, -0.8286],
         [ 0.1606,  1.4451,  0.5612]],

        [[ 0.0915, -0.2234,  0.3309],
         [ 0.3672,  0.7078, -0.4527]],

        [[-2.3169, -0.6216, -1.5576],
         [ 0.1754, -1.0759, -0.7718]],

        [[-0.2168, -0.5920, -0.9224],
         [ 1.3852,  0.5357, -0.1722]]])
>>> torch.permute(x, (1, 2, 0))
tensor([[[ 0.2415,  1.8113],
         [-1.1109,  0.1606],
         [ 0.0915,  0.3672],
         [-2.3169,  0.1754],
         [-0.2168,  1.3852]],

        [[-1.3847, -0.4459],
         [-0.8712,  1.4451],
         [-0.2234,  0.7078],
         [-0.6216, -1.0759],
         [-0.5920,  0.5357]],

        [[-0.0631,  1.1754],
         [-0.8286,  0.5612],
         [ 0.3309, -0.4527],
         [-1.5576, -0.7718],
         [-0.9224, -0.1722]]])
```


# [TORCH.TENSOR.VIEW](https://pytorch.org/docs/stable/generated/torch.Tensor.view.html#torch-tensor-view)
> Returns a new tensor with the same data as the self tensor but of a different shape.

```python
>>> t = torch.rand(4, 4)
>>> t
tensor([[0.0874, 0.0041, 0.1088, 0.1637],
        [0.7025, 0.6790, 0.9155, 0.2418],
        [0.1591, 0.7653, 0.2979, 0.8035],
        [0.3813, 0.7860, 0.1115, 0.2477]])
>>> t.view(2, 8)
tensor([[0.0874, 0.0041, 0.1088, 0.1637, 0.7025, 0.6790, 0.9155, 0.2418],
        [0.1591, 0.7653, 0.2979, 0.8035, 0.3813, 0.7860, 0.1115, 0.2477]])
>>> t.view(1, 16)
tensor([[0.0874, 0.0041, 0.1088, 0.1637, 0.7025, 0.6790, 0.9155, 0.2418, 0.1591,
         0.7653, 0.2979, 0.8035, 0.3813, 0.7860, 0.1115, 0.2477]])
>>> t.view(8, 2)
tensor([[0.0874, 0.0041],
        [0.1088, 0.1637],
        [0.7025, 0.6790],
        [0.9155, 0.2418],
        [0.1591, 0.7653],
        [0.2979, 0.8035],
        [0.3813, 0.7860],
        [0.1115, 0.2477]])
```


# [TORCH.UNSQUEEZE](https://pytorch.org/docs/stable/generated/torch.unsqueeze.html#torch-unsqueeze)
> Returns a new tensor with a dimension of size one inserted at the specified position

```python
>>> t
tensor([[0.0874, 0.0041, 0.1088, 0.1637],
        [0.7025, 0.6790, 0.9155, 0.2418],
        [0.1591, 0.7653, 0.2979, 0.8035],
        [0.3813, 0.7860, 0.1115, 0.2477]])
>>> torch.unsqueeze(t, 0)
tensor([[[0.0874, 0.0041, 0.1088, 0.1637],
         [0.7025, 0.6790, 0.9155, 0.2418],
         [0.1591, 0.7653, 0.2979, 0.8035],
         [0.3813, 0.7860, 0.1115, 0.2477]]])
>>> torch.unsqueeze(t, 0).size()
torch.Size([1, 4, 4])
>>> torch.unsqueeze(t, 1)
tensor([[[0.0874, 0.0041, 0.1088, 0.1637]],

        [[0.7025, 0.6790, 0.9155, 0.2418]],

        [[0.1591, 0.7653, 0.2979, 0.8035]],

        [[0.3813, 0.7860, 0.1115, 0.2477]]])
>>> torch.unsqueeze(t, 1).size()
torch.Size([4, 1, 4])
```


# [TORCH.CAT](https://pytorch.org/docs/stable/generated/torch.cat.html#torch-cat)
> Concatenates the given sequence of seq tensors in the given dimension. All tensors must either have the same shape (except in the concatenating dimension) or be empty.

```python
>>> x = torch.randn(2, 3)
>>> x
tensor([[ 0.6580, -1.0969, -0.4614],
        [-0.1034, -0.5790,  0.1497]])
>>> torch.cat((x, x, x), 0)
tensor([[ 0.6580, -1.0969, -0.4614],
        [-0.1034, -0.5790,  0.1497],
        [ 0.6580, -1.0969, -0.4614],
        [-0.1034, -0.5790,  0.1497],
        [ 0.6580, -1.0969, -0.4614],
        [-0.1034, -0.5790,  0.1497]])
>>> torch.cat((x, x, x), 1)
tensor([[ 0.6580, -1.0969, -0.4614,  0.6580, -1.0969, -0.4614,  0.6580,
         -1.0969, -0.4614],
        [-0.1034, -0.5790,  0.1497, -0.1034, -0.5790,  0.1497, -0.1034,
         -0.5790,  0.1497]])
```
